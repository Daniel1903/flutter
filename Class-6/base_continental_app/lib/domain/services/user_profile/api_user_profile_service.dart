import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:continental_app/utils/url_mixin.dart';

import 'package:continental_app/utils/constants.dart';

import 'package:continental_app/domain/services/user_profile/iuser_profile_service.dart';

import '../../models/models.dart';

class APIUserProfileService with UriMixin implements IUserProfileService {
  final http.Client client;
  final headers = {
    'Content-Type': 'application/json',
  };

  APIUserProfileService(this.client);

  @override
  Future<UserModel> getUser() async {
    try {
      final url = getUri('user/653c20ac96680964f7286ccb');
      var response = await http.get(url);
      final dynamic result = json.decode(response.body);
      return UserModel.fromJson(result);
    } catch (exception) {
      throw ErrorServiceModel(kGenericError);
    }
  }

  @override
  Future<bool> updateUser(Map<String, dynamic> body) async {
    try {
      final url = getUri('user/653c20ac96680964f7286ccb');
      var response = await http.patch(
        url,
        headers: headers,
        body: json.encode(body),
      );
      return response.statusCode == 200;
    } catch (exception) {
      throw ErrorServiceModel(kGenericError);
    }
  }
}
