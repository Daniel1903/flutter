import '../../models/models.dart';

abstract class IUserProfileService {
  Future<UserModel> getUser();

  Future<bool> updateUser(Map<String, dynamic> body);
}
