import 'package:http/http.dart';

import 'package:continental_app/presentation/pages/user_profile/user_profile_view_model.dart';

import '../../../domain/services/services.dart';

class UserProfileBuilder {
  static UserProfileViewModel builder() {
    IUserProfileService userProfileService = APIUserProfileService(Client());
    return UserProfileViewModel(userProfileService);
  }
}
