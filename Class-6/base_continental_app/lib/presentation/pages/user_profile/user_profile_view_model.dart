import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../domain/models/models.dart';
import '../../../domain/services/services.dart';

class UserProfileViewModel extends ChangeNotifier {
  IUserProfileService userProfileService;
  bool _loading = false;
  UserModel? _user;
  Uint8List? _picture;
  final defaultLocation = const LatLng(-12.0593574, -77.1127197);

  bool get loading => _loading;
  Uint8List? get picture => _picture;
  UserModel? get user => _user;
  MapModel get mapModel => MapModel(
        _user?.latitude ?? defaultLocation.latitude,
        _user?.longitude ?? defaultLocation.longitude,
        '',
      );

  UserProfileViewModel(this.userProfileService) {
    fetchUserProfile();
  }

  fetchUserProfile() async {
    _setLoading(true);
    _user = await userProfileService.getUser();
    if (_user?.avatarImage != null) {
      _picture = base64Decode(_user!.avatarImage!);
    }
    _setLoading(false);
  }

  Future<bool> updateUserProfile() async {
    _setLoading(true);
    if (_picture == null && _user != null) {
      return false;
    }
    _user!.avatarImage = await _preparePicture(_picture!);
    final result = await userProfileService.updateUser(_user!.toJson());
    _setLoading(false);
    return result;
  }

  setPicture(Uint8List? file) {
    _picture = file;
    notifyListeners();
  }

  setLocation(MapModel mapModel) {
    _user?.address = mapModel.address;
    _user?.latitude = mapModel.latitude;
    _user?.longitude = mapModel.longitude;
    notifyListeners();
  }

  _setLoading(bool loading) async {
    _loading = loading;
    notifyListeners();
  }

  Future<String> _preparePicture(Uint8List picture) async {
    final pictureCompressed = await FlutterImageCompress.compressWithList(
      picture,
      minHeight: 960,
      minWidth: 540,
      quality: 10,
      rotate: 0,
    );
    return base64Encode(pictureCompressed);
  }
}
