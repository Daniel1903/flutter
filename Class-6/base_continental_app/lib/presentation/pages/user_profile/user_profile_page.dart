import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:continental_app/presentation/pages/user_profile/user_profile_view_model.dart';

import 'package:continental_app/presentation/common_components/app_loading.dart';
import 'package:continental_app/presentation/pages/user_profile/widgets/avatar.dart';

import '../../../domain/models/models.dart';

class UserProfilePage extends StatefulWidget {
  const UserProfilePage({super.key});

  @override
  State<UserProfilePage> createState() {
    return _UserProfilePageState();
  }
}

class _UserProfilePageState extends State<UserProfilePage> {
  final _nameController = TextEditingController();
  final _addressController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    UserProfileViewModel userProfileViewModel =
        context.watch<UserProfileViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Perfil de Usuario'),
        backgroundColor: const Color.fromARGB(255, 196, 59, 59),
      ),
      body: _ui(context, userProfileViewModel),
    );
  }

  _ui(BuildContext context, UserProfileViewModel userProfileViewModel) {
    if (userProfileViewModel.loading) {
      return const AppLoading();
    }

    return Padding(
      padding: const EdgeInsets.all(16),
      child: Center(
        child: Column(
          children: [
            Avatar(picture: userProfileViewModel.picture),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromARGB(255, 196, 59, 59)),
              onPressed: () async {
                await availableCameras().then(
                  (value) => _goToCamera(userProfileViewModel, value),
                );
              },
              child: const Text('Update avatar'),
            ),
            const Divider(
              height: 20,
              color: Color.fromARGB(255, 211, 206, 206),
              thickness: 5,
            ),
            TextField(
              controller: _nameController
                ..text = userProfileViewModel.user?.name ?? '',
              decoration: const InputDecoration(
                label: Text('Nombre del usuario:'),
              ),
              onChanged: (text) => userProfileViewModel.user?.name = text,
            ),
            TextField(
              controller: _addressController
                ..text = userProfileViewModel.user?.address ?? '',
              decoration: InputDecoration(
                labelText: "Dirección",
                suffixIcon: InkWell(
                  onTap: () {
                    _goToMap(userProfileViewModel);
                  },
                  child: Tab(icon: Image.asset('assets/images/map.png')),
                ),
              ),
              onChanged: (text) => userProfileViewModel.user?.address = text,
            ),
            const SizedBox(height: 30),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromARGB(255, 196, 59, 59)),
              onPressed: () async {
                final result = await userProfileViewModel.updateUserProfile();
                if (!context.mounted) {
                  return;
                }
                _showSnackbar(result);
              },
              child: const Text('Update user'),
            ),
          ],
        ),
      ),
    );
  }

  _goToCamera(
    UserProfileViewModel userProfileViewModel,
    List<CameraDescription> cameras,
  ) async {
    final picture = await Navigator.pushNamed(
      context,
      '/camera',
      arguments: cameras,
    );
    if (picture is Uint8List) {
      userProfileViewModel.setPicture(picture);
    }
  }

  _goToMap(UserProfileViewModel userProfileViewModel) async {
    final mapModel = await Navigator.pushNamed(
      context,
      '/map',
      arguments: userProfileViewModel.mapModel,
    );
    if (mapModel is MapModel) {
      userProfileViewModel.setLocation(mapModel);
    }
  }

  _showSnackbar(bool result) {
    if (result) {
      const snackBar = SnackBar(
        content: Text('Success'),
        backgroundColor: Colors.lightGreen,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
