import 'package:http/http.dart';

import 'package:continental_app/presentation/pages/courses/courses_view_model.dart';
import '../../../domain/services/services.dart';

class CoursesBuilder {
  static CoursesViewModel builder() {
    ICourseService service = APICourseService(Client());
    // ICourseService service = BDCourseService();
    return CoursesViewModel(service);
  }
}
