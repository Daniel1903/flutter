import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:continental_app/presentation/pages/courses/courses_view_model.dart';
import 'package:continental_app/presentation/pages/courses/widgets/courses_item.dart';

import '../../../../domain/models/models.dart';

class CoursesList extends StatelessWidget {
  const CoursesList(this.courses, this.onSelect, {super.key});

  final List<CourseModel> courses;
  final void Function() onSelect;

  @override
  Widget build(BuildContext context) {
    CoursesViewModel coursesViewModel = context.watch<CoursesViewModel>();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListView.builder(
        itemCount: coursesViewModel.courseListModel.length,
        itemBuilder: (ctx, index) => Dismissible(
          background: Container(
            color: Colors.redAccent,
            margin: const EdgeInsets.symmetric(horizontal: 16),
          ),
          onDismissed: (direction) {
            coursesViewModel.removeCourse(courses[index]);
          },
          key: ValueKey(courses[index]),
          child: CoursesItem(courses[index], onSelect),
        ),
      ),
    );
  }
}
