import 'dart:convert';
import 'package:flutter_keychain/flutter_keychain.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:continental_app/utils/url_mixin.dart';
import 'package:continental_app/utils/constants.dart';

import '../../models/models.dart';
import 'iuser_profile_service.dart';

class APIUserProfileService with UriMixin implements IUserProfileService {
  final http.Client client;
  final headers = {
    'Content-Type': 'application/json',
  };

  APIUserProfileService(this.client);

  Future<String> _getUserId() async {
    final isFirstInstall = await _isFirstInstall();
    if (isFirstInstall) {
      const userId = '653c20ac96680964f7286ccb';
      await FlutterKeychain.put(key: kUserId, value: userId);
      return userId;
    } else {
      final userId = await FlutterKeychain.get(key: kUserId) ?? '';
      return userId;
    }
  }

  Future<bool> _isFirstInstall() async {
    final newUserDefault = await SharedPreferences.getInstance();
    final isFirstInstall = newUserDefault.getBool(kIsFirstInstall);
    if (isFirstInstall ?? true) {
      newUserDefault.setBool(kIsFirstInstall, false);
    }
    return isFirstInstall ?? true;
  }

  @override
  Future<UserModel> getUser() async {
    try {
      final userId = await _getUserId();
      final url = getUri('user/$userId');
      var response = await client.get(url);
      final dynamic result = json.decode(response.body);
      return UserModel.fromJson(result);
    } catch (exception) {
      throw ErrorServiceModel(kGenericError);
    }
  }

  @override
  Future<bool> updateUser(Map<String, dynamic> body) async {
    try {
      final userId = await _getUserId();
      final url = getUri('user/$userId');
      var response = await client.patch(
        url,
        headers: headers,
        body: json.encode(body),
      );
      return response.statusCode == 200;
    } catch (exception) {
      throw ErrorServiceModel(kGenericError);
    }
  }
}
