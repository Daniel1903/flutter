import 'package:http/http.dart';

import '../../../domain/services/services.dart';
import 'user_profile_view_model.dart';

class UserProfileBuilder {
  static UserProfileViewModel builder() {
    IUserProfileService userProfileService = APIUserProfileService(Client());
    return UserProfileViewModel(userProfileService);
  }
}
