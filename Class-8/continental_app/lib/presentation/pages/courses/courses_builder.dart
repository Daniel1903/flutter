import 'package:http/http.dart';

import '../../../domain/services/services.dart';
import 'courses_view_model.dart';

class CoursesBuilder {
  static CoursesViewModel builder() {
    ICourseService service = APICourseService(Client());
    // ICourseService service = BDCourseService();
    return CoursesViewModel(service);
  }
}
