import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../domain/models/models.dart';
import '../../common_components/common_components.dart';
import 'courses_view_model.dart';

class CourseDetailPage extends StatefulWidget {
  const CourseDetailPage({super.key});

  @override
  State<CourseDetailPage> createState() {
    return _CourseDetailPageState();
  }
}

class _CourseDetailPageState extends State<CourseDetailPage> {
  final _nameController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  void _submitData(
    CoursesViewModel coursesViewModel,
    BuildContext context,
  ) async {
    bool isSaved = await coursesViewModel.saveCourse();
    if (!context.mounted) {
      return;
    }
    if (!isSaved) {
      showErrorDialog(context);
      return;
    }
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    CoursesViewModel coursesViewModel = context.watch<CoursesViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalle del curso '),
      ),
      body: _ui(coursesViewModel),
    );
  }

  _ui(CoursesViewModel coursesViewModel) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          TextField(
            style: Theme.of(context).textTheme.bodySmall,
            controller: _nameController
              ..text = coursesViewModel.detailCourse?.title ?? '',
            maxLength: 100,
            decoration: const InputDecoration(
              label: Text('Nombre del curso:'),
            ),
            onChanged: (text) => coursesViewModel.detailCourse?.title = text,
          ),
          Row(
            children: [
              const Text('Tipo de categoria:'),
              const SizedBox(width: 10),
              DropdownButton(
                style: Theme.of(context).textTheme.bodySmall,
                items: CategoryCourseModel.values
                    .map((category) => DropdownMenuItem(
                          value: category,
                          child: Text(category.name.toUpperCase()),
                        ))
                    .toList(),
                value: coursesViewModel.detailCourse?.category ??
                    CategoryCourseModel.web,
                onChanged: (value) {
                  if (value == null) {
                    return;
                  }
                  setState(() {
                    coursesViewModel.detailCourse?.category = value;
                  });
                },
              ),
            ],
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: () async {
              _submitData(coursesViewModel, context);
            },
            child: Text(
                coursesViewModel.isNewCourse ? 'Save Course' : 'Update Course'),
          ),
        ],
      ),
    );
  }
}
