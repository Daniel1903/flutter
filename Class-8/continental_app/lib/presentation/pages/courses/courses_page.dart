import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common_components/common_components.dart';
import 'courses_view_model.dart';
import 'widgets/courses_header.dart';
import 'widgets/courses_list.dart';

class CoursesPage extends StatefulWidget {
  const CoursesPage({super.key});

  @override
  State<CoursesPage> createState() {
    return _CoursesPageState();
  }
}

class _CoursesPageState extends State<CoursesPage> {
  @override
  void initState() {
    super.initState();
  }

  List<Widget>? _barActions(
      CoursesViewModel coursesViewModel, BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/userProfile');
        },
        icon: const Icon(Icons.person),
      )
    ];
  }

  _navigateToDetail(BuildContext context) {
    // Navigator.of(context).pushNamed('/detail', arguments: );
    Navigator.of(context).pushNamed('/detail');
  }

  @override
  Widget build(BuildContext context) {
    CoursesViewModel coursesViewModel = context.watch<CoursesViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Instituto Continental'),
        actions: _barActions(coursesViewModel, context),
      ),
      body: _ui(coursesViewModel),
    );
  }

  _ui(CoursesViewModel coursesViewModel) {
    if (coursesViewModel.loading) {
      return const AppLoading();
    }

    if (coursesViewModel.courseError != null) {
      return Container();
    }

    return Column(
      children: [
        const CoursesHeader(),
        Expanded(
          child: CoursesList(
            coursesViewModel.courseListModel,
            () {
              _navigateToDetail(context);
            },
          ),
        ),
        FloatingActionButton(
          onPressed: () {
            coursesViewModel.setNewCourse();
            _navigateToDetail(context);
          },
          child: const Icon(Icons.add),
        )
      ],
    );
  }
}
