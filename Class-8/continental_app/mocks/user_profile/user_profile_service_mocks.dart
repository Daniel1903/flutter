import 'package:continental_app/domain/models/models.dart';
import 'package:continental_app/domain/services/services.dart';

class UserProfileServiceMock implements IUserProfileService {
  bool getUserWasCalled = false;
  bool updateUserWasCalled = false;
  UserModel? userResult;
  bool? updateUserResult;

  @override
  Future<UserModel> getUser() async {
    getUserWasCalled = true;
    if (userResult != null) {
      return userResult!;
    } else {
      throw Error();
    }
  }

  @override
  Future<bool> updateUser(Map<String, dynamic> body) async {
    updateUserWasCalled = true;
    if (updateUserResult != null) {
      return updateUserResult!;
    } else {
      throw Error();
    }
  }
}
