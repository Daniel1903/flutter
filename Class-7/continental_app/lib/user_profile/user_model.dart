class UserModel {
  String name;
  String address;
  double latitude;
  double longitude;
  String? avatarImage;

  UserModel({
    required this.name,
    required this.latitude,
    required this.longitude,
    required this.address,
    this.avatarImage,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    if (json
        case {
          'name': String name,
          'address': String address,
          'latitude': double latitude,
          'longitude': double longitude
        }) {
      return UserModel(
        name: name,
        address: address,
        latitude: latitude,
        longitude: longitude,
        avatarImage: json['photo'] as String?,
      );
    } else {
      throw const FormatException('Invalid JSON');
    }
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'address': address,
        'latitude': latitude,
        'longitude': longitude,
        'photo': avatarImage,
      };
}
