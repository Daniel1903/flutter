import 'package:continental_app/user_profile/user_model.dart';

abstract class IUserProfileService {
  Future<UserModel> getUser();

  Future<bool> updateUser(Map<String, dynamic> body);
}
