import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:continental_app/maps/map_model.dart';

import 'package:continental_app/user_profile/user_profile_view_model.dart';

import 'package:continental_app/common_components/app_loading.dart';
import 'package:continental_app/user_profile/widgets/avatar.dart';

class UserProfilePage extends StatefulWidget {
  const UserProfilePage({super.key});

  @override
  State<UserProfilePage> createState() {
    return _UserProfilePageState();
  }
}

class _UserProfilePageState extends State<UserProfilePage> {
  final _nameController = TextEditingController();
  final _addressController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final viewModel =
          Provider.of<UserProfileViewModel>(context, listen: false);
      viewModel.fetchUserProfile();
    });
  }

  @override
  Widget build(BuildContext context) {
    UserProfileViewModel userProfileViewModel =
        context.watch<UserProfileViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Perfil de Usuario'),
      ),
      body: _ui(context, userProfileViewModel),
    );
  }

  _ui(BuildContext context, UserProfileViewModel userProfileViewModel) {
    final width = MediaQuery.of(context).size.width;
    if (userProfileViewModel.loading) {
      return const AppLoading();
    }

    return Padding(
      padding: const EdgeInsets.all(16),
      child: width < 600
          ? _vertical(userProfileViewModel)
          : _horizontal(userProfileViewModel),
    );
  }

  _horizontal(UserProfileViewModel userProfileViewModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _uiAvatar(userProfileViewModel),
        const SizedBox(width: 20),
        Expanded(
          child: _uiForm(userProfileViewModel),
        )
      ],
    );
  }

  _vertical(UserProfileViewModel userProfileViewModel) {
    return Center(
      child: Column(
        children: [
          _uiAvatar(userProfileViewModel),
          const Divider(
            height: 20,
            color: Color.fromARGB(255, 211, 206, 206),
            thickness: 5,
          ),
          _uiForm(userProfileViewModel),
        ],
      ),
    );
  }

  _uiAvatar(UserProfileViewModel userProfileViewModel) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Avatar(picture: userProfileViewModel.picture),
        ElevatedButton(
          onPressed: () async {
            await availableCameras().then(
              (value) => _goToCamera(userProfileViewModel, value),
            );
          },
          child: const Text('Update avatar'),
        )
      ],
    );
  }

  _uiForm(UserProfileViewModel userProfileViewModel) {
    return Column(
      children: [
        TextField(
          style: Theme.of(context).textTheme.bodySmall,
          controller: _nameController
            ..text = userProfileViewModel.user?.name ?? '',
          decoration: const InputDecoration(
            label: Text('Nombre del usuario:'),
          ),
          onChanged: (text) => userProfileViewModel.user?.name = text,
        ),
        TextField(
          style: Theme.of(context).textTheme.bodySmall,
          controller: _addressController
            ..text = userProfileViewModel.user?.address ?? '',
          decoration: InputDecoration(
            labelText: "Dirección",
            suffixIcon: InkWell(
              onTap: () {
                _goToMap(userProfileViewModel);
              },
              child: Tab(icon: Image.asset('assets/images/map.png')),
            ),
          ),
          onChanged: (text) => userProfileViewModel.user?.address = text,
        ),
        const SizedBox(height: 30),
        ElevatedButton(
          onPressed: () async {
            final result = await userProfileViewModel.updateUserProfile();
            if (!context.mounted) {
              return;
            }
            _showSnackbar(result);
          },
          child: const Text('Update user'),
        ),
      ],
    );
  }

  _goToCamera(
    UserProfileViewModel userProfileViewModel,
    List<CameraDescription> cameras,
  ) async {
    final picture = await Navigator.pushNamed(
      context,
      '/camera',
      arguments: cameras,
    );
    if (picture is Uint8List) {
      userProfileViewModel.setPicture(picture);
    }
  }

  _goToMap(UserProfileViewModel userProfileViewModel) async {
    final mapModel = await Navigator.pushNamed(
      context,
      '/map',
      arguments: userProfileViewModel.mapModel,
    );
    if (mapModel is MapModel) {
      userProfileViewModel.setLocation(mapModel);
    }
  }

  _showSnackbar(bool result) {
    if (result) {
      const snackBar = SnackBar(
        content: Text('Success'),
        backgroundColor: Colors.lightGreen,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
