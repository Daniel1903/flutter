import 'dart:typed_data';

import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final Uint8List? picture;

  const Avatar({super.key, this.picture});

  @override
  Widget build(BuildContext context) {
    bool isDarkMode =
        MediaQuery.of(context).platformBrightness == Brightness.dark;
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.transparent,
        shape: BoxShape.circle,
        border: Border.all(
          color: isDarkMode
              ? Theme.of(context).colorScheme.inverseSurface
              : Theme.of(context).colorScheme.primary,
          width: 5,
        ),
      ),
      child: ClipOval(
        child: SizedBox.fromSize(
          size: const Size.fromRadius(80),
          child: picture == null ? _defaultAvatar() : _imageAvatar(picture!),
        ),
      ),
    );
  }

  _defaultAvatar() {
    return Image.asset(
      'assets/images/user.png',
      fit: BoxFit.cover,
    );
  }

  _imageAvatar(Uint8List picture) {
    return Image.memory(picture, fit: BoxFit.cover);
  }
}
