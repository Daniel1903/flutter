import 'package:http/http.dart';

import 'package:continental_app/user_profile/services/api_user_profile_service.dart';
import 'package:continental_app/user_profile/services/iuser_profile_service.dart';

import 'package:continental_app/user_profile/user_profile_view_model.dart';

class UserProfileBuilder {
  static UserProfileViewModel builder() {
    IUserProfileService userProfileService = APIUserProfileService(Client());
    return UserProfileViewModel(userProfileService);
  }
}
