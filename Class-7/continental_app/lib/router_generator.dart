import 'package:flutter/material.dart';
import 'package:camera/camera.dart';

import 'package:continental_app/maps/map_model.dart';

import 'package:continental_app/camera/camera_page.dart';
import 'package:continental_app/courses/courses_page.dart';
import 'package:continental_app/courses/course_detail_page.dart';
import 'package:continental_app/maps/maps_page.dart';
import 'package:continental_app/user_profile/user_profile_page.dart';

class RouterGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final arguments = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const CoursesPage());
      case '/detail':
        return MaterialPageRoute(builder: (_) => const CourseDetailPage());
      case '/userProfile':
        return MaterialPageRoute(builder: (_) => const UserProfilePage());
      case '/camera':
        if (arguments is List<CameraDescription>) {
          return MaterialPageRoute(
              builder: (_) => CameraPage(cameras: arguments));
        } else {
          return _errorRoute();
        }
      case '/map':
        if (arguments is MapModel) {
          return MaterialPageRoute(
              builder: (_) => LocationMap(mapModel: arguments));
        } else {
          return _errorRoute();
        }
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
