import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:continental_app/router_generator.dart';

import 'package:continental_app/courses/courses_builder.dart';
import 'package:continental_app/maps/map_builder.dart';
import 'package:continental_app/user_profile/user_profile_builder.dart';

import 'package:continental_app/utils/color_theme.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CoursesBuilder.builder()),
        ChangeNotifierProvider(create: (_) => UserProfileBuilder.builder()),
        ChangeNotifierProvider(create: (_) => MapBuilder.builder()),
      ],
      child: MaterialApp(
        initialRoute: '/',
        onGenerateRoute: RouterGenerator.generateRoute,
        theme: ColorTheme.fetchColorScheme(),
        darkTheme: ColorTheme.fetchDarkColorScheme(),
      ),
    );
  }
}
