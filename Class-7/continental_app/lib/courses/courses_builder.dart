import 'package:http/http.dart';

import 'package:continental_app/courses/services/bd_course_service.dart';
import 'package:continental_app/courses/services/api_course_service.dart';
import 'package:continental_app/courses/services/icourse_service.dart';

import 'package:continental_app/courses/courses_view_model.dart';

class CoursesBuilder {
  static CoursesViewModel builder() {
    ICourseService service = APICourseService(Client());
    // ICourseService service = BDCourseService();
    return CoursesViewModel(service);
  }
}
