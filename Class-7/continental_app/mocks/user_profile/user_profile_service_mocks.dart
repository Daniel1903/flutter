import 'package:continental_app/user_profile/user_model.dart';

import 'package:continental_app/user_profile/services/iuser_profile_service.dart';

class UserProfileServiceMock implements IUserProfileService {
  bool getUserWasCalled = false;
  bool updateUserWasCalled = false;
  UserModel? userResult;
  bool? updateUserResult;

  @override
  Future<UserModel> getUser() async {
    getUserWasCalled = true;
    if (userResult != null) {
      return userResult!;
    } else {
      throw Error();
    }
  }

  @override
  Future<bool> updateUser(Map<String, dynamic> body) async {
    updateUserWasCalled = true;
    if (updateUserResult != null) {
      return updateUserResult!;
    } else {
      throw Error();
    }
  }
}
