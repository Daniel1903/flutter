import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;

import 'package:continental_app/user_profile/services/api_user_profile_service.dart';
import 'package:continental_app/user_profile/user_model.dart';
import 'package:continental_app/courses/models/error_service_model.dart';

import '../../mocks/http_client.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  late MockClient mockClient;
  late APIUserProfileService sut;
  const getUserUrl = 'user/653c20ac96680964f7286ccb';

  setUp(() {
    mockClient = MockClient();
    sut = APIUserProfileService(mockClient);
  });

  group('Get User', () {
    final dummyUser = UserModel(
      name: 'Daniel',
      latitude: -11.8125462,
      longitude: -77.1212482,
      address: 'Av. Carretera Panamericana Norte Km 39 - Ancón',
    );
    const jsonUserSuccess = """ {
      "id": "653c20ac96680964f7286ccb",
      "dob": "1985-02-04T23:00:00.000Z",
      "name": "Daniel",
      "email": "cristiano.ronaldo@realmadrid.com",
      "address": "Av. Carretera Panamericana Norte Km 39 - Ancón",
      "latitude": -11.8125462,
      "longitude": -77.1212482
    }
    """;

    test('Should return the user', () async {
      // GIVEN
      final url = sut.getUri(getUserUrl);
      when(mockClient.get(url))
          .thenAnswer((_) async => http.Response(jsonUserSuccess, 200));

      // WHEN
      final result = await sut.getUser();

      // THEN
      expect(result.name, dummyUser.name);
      expect(result.latitude, dummyUser.latitude);
      expect(result.longitude, dummyUser.longitude);
      expect(result.address, dummyUser.address);
    });

    test('Should throw an error when status code is not 200', () async {
      // GIVEN
      final url = sut.getUri(getUserUrl);
      when(mockClient.get(url)).thenAnswer((_) async => http.Response('', 404));

      // WHEN
      final call = sut.getUser();

      // THEN
      expect(() => call, throwsA(isA<ErrorServiceModel>()));
      verify(mockClient.get(url));
    });
  });

  group('Update User', () {
    const headers = {'Content-Type': 'application/json'};
    final dummyUser = UserModel(
      name: 'CR7',
      latitude: -11.8125462,
      longitude: -77.1212482,
      address: 'Av. Carretera Panamericana',
    );

    test('Should update the user', () async {
      // GIVEN
      final url = sut.getUri(getUserUrl);
      final body = json.encode(dummyUser.toJson());
      when(mockClient.patch(url, headers: headers, body: body))
          .thenAnswer((_) async => http.Response('', 200));

      // WHEN
      final result = await sut.updateUser(dummyUser.toJson());

      // THEN
      expect(result, isTrue);
    });

    test('Should throw an error when status code is not 200', () async {
      // GIVEN
      final url = sut.getUri(getUserUrl);
      final body = json.encode(dummyUser.toJson());
      when(mockClient.patch(url, headers: headers, body: body))
          .thenAnswer((_) async => http.Response('', 404));

      // WHEN
      final result = await sut.updateUser(dummyUser.toJson());

      // THEN
      expect(!result, isTrue);
    });
  });
}
