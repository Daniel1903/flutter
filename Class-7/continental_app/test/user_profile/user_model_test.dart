import 'package:continental_app/user_profile/user_model.dart';
import 'package:test/test.dart';

void main() {
  group('User Model', () {
    test('test init', () {
      final userModel = UserModel(
        name: 'Daniel',
        latitude: 12,
        longitude: 12,
        address: 'address',
      );
      expect(userModel.name, 'Daniel');
      expect(userModel.latitude, 12);
      expect(userModel.longitude, 12);
      expect(userModel.address, 'address');
    });

    test('test from json', () {
      final json = {
        'name': 'Daniel',
        'address': 'address',
        'latitude': 12.0,
        'longitude': 12.0,
        'photo': 'avatar',
      };
      final userModel = UserModel.fromJson(json);
      expect(userModel.name, 'Daniel');
      expect(userModel.address, 'address');
      expect(userModel.latitude, 12);
      expect(userModel.longitude, 12);
      expect(userModel.avatarImage, 'avatar');
    });

    test('test from json without photo', () {
      final json = {
        'name': 'Daniel',
        'address': 'address',
        'latitude': 12.0,
        'longitude': 12.0,
      };
      final userModel = UserModel.fromJson(json);
      expect(userModel.name, 'Daniel');
      expect(userModel.address, 'address');
      expect(userModel.latitude, 12);
      expect(userModel.longitude, 12);
      expect(userModel.avatarImage, isNull);
    });

    test('test model to json', () {
      final userModel = UserModel(
        name: 'Daniel',
        latitude: 12,
        longitude: 12,
        address: 'address',
      );
      final json = userModel.toJson();
      expect(json['name'], 'Daniel');
      expect(json['address'], 'address');
      expect(json['latitude'], 12);
      expect(json['longitude'], 12);
      expect(json['photo'], isNull);
    });

    test('test invalid json', () {
      final json = {
        'name': 'Daniel',
        'latitude': 12.0,
        'longitude': 12.0,
        'photo': 'avatar',
      };
      try {
        UserModel.fromJson(json);
        fail('expected invalid json');
      } catch (exception) {
        expect(exception is FormatException, isTrue);
      }
    });
  });
}
