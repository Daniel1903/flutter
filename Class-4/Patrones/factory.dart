class Person {
  final String name;
  final int age;

  Person._(this.name, this.age);

  // Constructor que retorna diferentes tipo de personas
  factory Person(String name, int age) {
    if (age < 18) {
      return Teenager(name, age);
    } else {
      return Adult(name, age);
    }
  }
}

// Sub clase que representa a un joven
class Teenager extends Person {
  Teenager(String name, int age) : super._(name, age);

  @override
  String toString() => 'Teenager: $name, $age years old';
}

// Sub clase que representa a un adulto
class Adult extends Person {
  Adult(String name, int age) : super._(name, age);

  @override
  String toString() => 'Adult: $name, $age years old';
}

void main() {
  var person1 = Person('Alice', 15);
  var person2 = Person('Bob', 25);

  print(person1);
  print(person2);
}
