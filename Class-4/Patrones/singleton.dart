class MySingleton {
  // Instancia única privada
  static MySingleton? _instance;

  // Constructor privado
  MySingleton._();

  // Método estático para obtener la instancia única
  static MySingleton get instancia {
    _instance ??= MySingleton._();
    return _instance!;
  }

  // Método de ejemplo
  void example() {
    print("Example Singleton");
  }
}

void main() {
  // Obtener la instancia del Singleton
  var singleton = MySingleton.instancia;

  // Llamar a un método en la instancia del Singleton
  singleton.example();

  // Intentar crear una nueva instancia (no debería ser posible)
  // var nuevoSingleton = MiSingleton(); // Esto daría un error, ya que el constructor es privado

  var otraInstancia = MySingleton.instancia;
  print(identical(singleton, otraInstancia));
}
