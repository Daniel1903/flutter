class Product {
  final String name;
  final String description;
  final double price;

  Product(this.name, this.description, this.price);

  void showDetail() {
    print("Nombre: $name");
    print("Descripción: $description");
    print("Precio: \$${price.toStringAsFixed(2)}");
  }
}

// Implementación del builder
class DefaultBuilderProduct {
  String? name;
  String? description;
  double? price;

  void buildName(String name) {
    this.name = name;
  }

  void buildDescription(String description) {
    this.description = description;
  }

  void buildPrice(double price) {
    this.price = price;
  }

  Product getProduct() {
    if (name == null || description == null || price == null) {
      throw Exception("Faltan datos para construir el producto");
    }
    return Product(name!, description!, price!);
  }
}

class Client {
  DefaultBuilderProduct builder;

  Client(this.builder);

  Product buildProduct() {
    builder.buildName("Producto");
    builder.buildDescription("Descripción del producto");
    builder.buildPrice(100.0);

    return builder.getProduct();
  }
}

void main() {
  // Usando el builder para construir el producto
  var builder = DefaultBuilderProduct();
  var client = Client(builder);
  var product = client.buildProduct();

  product.showDetail();
}
