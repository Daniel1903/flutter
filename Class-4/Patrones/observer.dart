// Interzas para definir metodos de agregar, elimiar
abstract class Subject {
  void addObserver(Observer observer);
  void removeObserver(Observer observer);
  void notifyObservers();
}

// Implementación de la interfaz
class ConcreteSubject implements Subject {
  List<Observer> _observers = [];
  // variable que se define, pero que se inicializa despues
  late String _state;

  String get state => _state;

  set state(String value) {
    _state = value;
    notifyObservers();
  }

  @override
  void addObserver(Observer observer) {
    _observers.add(observer);
  }

  @override
  void removeObserver(Observer observer) {
    _observers.remove(observer);
  }

  @override
  void notifyObservers() {
    for (var observer in _observers) {
      observer.update();
    }
  }
}

// Interzas para definir observadores
abstract class Observer {
  void update();
}

// Clase que implementa el observador
class ConcreteObserver implements Observer {
  final String _name;
  final ConcreteSubject _subject;

  ConcreteObserver(this._name, this._subject);

  @override
  void update() {
    print('$_name: Subject state changed to ${_subject.state}');
  }
}

void main() {
  // Creación de la clase que maneja observadores
  var subject = ConcreteSubject();

  // Creación de observadores
  var observer1 = ConcreteObserver('Observer 1', subject);
  var observer2 = ConcreteObserver('Observer 2', subject);
  subject.addObserver(observer1);
  subject.addObserver(observer2);

  // Cambiar estado
  // Ambos observadores se van actualizar
  subject.state = 'State 1';

  // Eliminamos 1 observador
  subject.removeObserver(observer2);

  // Cambiar estado
  // Solo un observador se va actualizar
  subject.state = 'State 2';
}
