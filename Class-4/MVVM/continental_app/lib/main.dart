import 'package:continental_app/presentation/courses/view_models/courses_view_model.dart';
import 'package:flutter/material.dart';

import 'package:continental_app/presentation/courses/views/courses.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CoursesViewModel()),
      ],
      child: const MaterialApp(
        home: Courses(),
      ),
    );
  }
}
