import 'dart:convert';
import 'package:flutter/services.dart';

import 'package:continental_app/utils/constants.dart';
import 'package:continental_app/domain/models/error_service_model.dart';
import 'package:continental_app/domain/models/course_model.dart';

class CourseService {
  static Future<List<Object>> fetchItems() async {
    try {
      await Future.delayed(const Duration(seconds: 1));
      final jsonString =
          await rootBundle.loadString('assets/data/courses.json');
      final List<dynamic> jsonList = json.decode(jsonString);
      return jsonList.map((item) => CourseModel.fromJson(item)).toList();
    } on ArgumentError {
      throw ErrorServiceModel(kParseError);
    } catch (ex) {
      throw ErrorServiceModel(kGenericError);
    }
  }
}
