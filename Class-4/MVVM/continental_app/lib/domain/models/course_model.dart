import 'package:uuid/uuid.dart';
import 'package:continental_app/domain/models/category_course_model.dart';

const uuid = Uuid();

class CourseModel {
  final String identifier;
  String title;
  CategoryCourseModel category;

  CourseModel({
    required this.title,
    required this.category,
  }) : identifier = uuid.v4();

  factory CourseModel.fromJson(Map<String, dynamic> json) {
    String stringCategory = json['category'] as String;
    return CourseModel(
      title: json['title'] as String,
      category: CategoryCourseModel.byName(stringCategory),
    );
  }
}
