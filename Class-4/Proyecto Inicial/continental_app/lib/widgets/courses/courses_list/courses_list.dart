import 'package:flutter/material.dart';

import 'package:continental_app/models/course.dart';

import 'package:continental_app/widgets/courses/courses_list/courses_item.dart';

class CoursesList extends StatelessWidget {
  const CoursesList(this.courses, this.onRemoveCourse, {super.key});

  final List<Course> courses;
  final void Function(Course course) onRemoveCourse;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListView.builder(
        itemCount: courses.length,
        itemBuilder: (ctx, index) => Dismissible(
          background: Container(
            color: Colors.redAccent,
            margin: const EdgeInsets.symmetric(horizontal: 16),
          ),
          onDismissed: (direction) {
            onRemoveCourse(courses[index]);
          },
          key: ValueKey(courses[index]),
          child: CoursesItem(courses[index]),
        ),
      ),
    );
  }
}
