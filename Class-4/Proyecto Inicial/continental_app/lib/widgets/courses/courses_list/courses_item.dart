import 'package:continental_app/models/category_course.dart';
import 'package:flutter/material.dart';

import 'package:continental_app/models/course.dart';

class CoursesItem extends StatelessWidget {
  const CoursesItem(this.course, {super.key});

  final Course course;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: [
          Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(course.category.icon),
            ),
          ),
          Text(course.title),
        ],
      ),
    );
  }
}
