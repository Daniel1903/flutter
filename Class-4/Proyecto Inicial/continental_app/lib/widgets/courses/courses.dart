import 'package:continental_app/widgets/new_course/new_course.dart';
import 'package:flutter/material.dart';

import 'package:continental_app/models/course.dart';
import 'package:continental_app/models/category_course.dart';

import 'package:continental_app/widgets/courses/courses_header.dart';
import 'package:continental_app/widgets/courses/courses_list/courses_list.dart';

class Courses extends StatefulWidget {
  const Courses({super.key});

  @override
  State<Courses> createState() {
    return _CoursesState();
  }
}

class _CoursesState extends State<Courses> {
  final List<Course> _registeredCourses = [
    Course(
      title: 'Desarrollo de aplicaciones móviles',
      category: CategoryCourse.movil,
    ),
    Course(
      title: 'Desarrollo de aplicaciones web',
      category: CategoryCourse.web,
    ),
  ];

  void _removeCourse(Course course) {
    setState(() {
      _registeredCourses.remove(course);
    });
  }

  void _addCourse(Course course) {
    setState(() {
      _registeredCourses.add(course);
    });
  }

  void _openAddCourseOverlay() {
    showModalBottomSheet(
        useSafeArea: true,
        isScrollControlled: true,
        context: context,
        builder: (ctx) => NewCourse(_addCourse));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Instituto Continental'),
        backgroundColor: const Color.fromARGB(255, 196, 59, 59),
        actions: [
          IconButton(
            onPressed: _openAddCourseOverlay,
            icon: const Icon(Icons.add),
          )
        ],
      ),
      body: Column(
        children: [
          const CoursesHeader(),
          Expanded(
            child: CoursesList(_registeredCourses, _removeCourse),
          )
        ],
      ),
    );
  }
}
