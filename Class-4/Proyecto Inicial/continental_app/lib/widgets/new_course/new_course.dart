import 'package:flutter/material.dart';

import 'package:continental_app/models/course.dart';
import 'package:continental_app/models/category_course.dart';

import 'package:continental_app/widgets/new_course/new_course_dialog.dart';

class NewCourse extends StatefulWidget {
  const NewCourse(this.onAddCourse, {super.key});

  final void Function(Course course) onAddCourse;

  @override
  State<NewCourse> createState() {
    return _NewCourseState();
  }
}

class _NewCourseState extends State<NewCourse> {
  final _nameController = TextEditingController();
  CategoryCourse _selectedCategory = CategoryCourse.web;

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  void _submitExpenseData() {
    if (_nameController.text.trim().isEmpty) {
      showErrorDialog(context);
      return;
    }

    Course newCourse = Course(
      title: _nameController.text,
      category: _selectedCategory,
    );
    widget.onAddCourse(newCourse);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          TextField(
            controller: _nameController,
            maxLength: 100,
            decoration: const InputDecoration(
              label: Text('Nombre del curso:'),
            ),
          ),
          Row(
            children: [
              const Text('Tipo de categoria:'),
              const SizedBox(width: 10),
              DropdownButton(
                value: _selectedCategory,
                items: CategoryCourse.values
                    .map((category) => DropdownMenuItem(
                          value: category,
                          child: Text(category.name.toUpperCase()),
                        ))
                    .toList(),
                onChanged: (value) {
                  if (value == null) {
                    return;
                  }
                  setState(() {
                    _selectedCategory = value;
                  });
                },
              ),
            ],
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: const Color.fromARGB(255, 196, 59, 59)),
            onPressed: _submitExpenseData,
            child: const Text('Save Expense'),
          )
        ],
      ),
    );
  }
}
