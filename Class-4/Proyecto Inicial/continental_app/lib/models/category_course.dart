import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

const uuid = Uuid();

enum CategoryCourse { movil, web, cloud }

extension CategoryCourseExtension on CategoryCourse {
  IconData get icon {
    switch (this) {
      case CategoryCourse.movil:
        return Icons.mobile_friendly;
      case CategoryCourse.web:
        return Icons.web;
      case CategoryCourse.cloud:
        return Icons.filter_drama_outlined;
    }
  }
}
