import 'package:continental_app/models/category_course.dart';

class Course {
  final String identifier;
  final String title;
  final CategoryCourse category;

  Course({
    required this.title,
    required this.category,
  }) : identifier = uuid.v4();
}
