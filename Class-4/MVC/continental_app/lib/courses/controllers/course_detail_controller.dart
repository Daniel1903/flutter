import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:continental_app/courses/models/course_model.dart';
import 'package:continental_app/courses/models/category_course_model.dart';

import 'package:continental_app/courses/controllers/courses_controller.dart';

class CourseDetailController extends GetxController {
  CourseModel? courseDetail;
  final _nameController = TextEditingController();
  Rx<CategoryCourseModel?> _selectedCategory = Rxn<CategoryCourseModel>();

  Rx<CategoryCourseModel?> get selectedCategory => _selectedCategory;
  TextEditingController get nameController => _nameController;
  RxList<CategoryCourseModel> get categories => CategoryCourseModel.values.obs;

  static CourseDetailController get to => Get.find();

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  loadCourse(CourseModel? courseDetail) {
    this.courseDetail = courseDetail ??
        CourseModel(
          title: '',
          category: CategoryCourseModel.web,
        );
    _selectedCategory = this.courseDetail!.category.obs;
    _nameController.text = this.courseDetail!.title;
  }

  onChangeCategory(CategoryCourseModel? category) {
    if (category != null) {
      _selectedCategory.value = category;
    }
  }

  Future<bool> onSaveCategory() async {
    if (courseDetail == null || _nameController.text.trim().isEmpty) {
      return false;
    }
    courseDetail!.title = _nameController.text;
    courseDetail!.category = _selectedCategory.value ?? CategoryCourseModel.web;
    await CoursesController.to.saveCourse(courseDetail!);
    return true;
  }
}
