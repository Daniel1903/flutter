import 'package:get/get.dart';

import 'package:continental_app/courses/models/course_model.dart';
import 'package:continental_app/courses/models/category_course_model.dart';

class CoursesController extends GetxController {
  final RxList<CourseModel> _courseListModel = [
    CourseModel(
      title: 'Desarrollo de aplicaciones móviles',
      category: CategoryCourseModel.movil,
    ),
    CourseModel(
      title: 'Desarrollo de aplicaciones web',
      category: CategoryCourseModel.web,
    ),
    CourseModel(
      title: 'Desarrollo de aplicaciones web 2',
      category: CategoryCourseModel.web,
    ),
    CourseModel(
      title: 'Desarrollo de aplicaciones web 3',
      category: CategoryCourseModel.web,
    ),
    CourseModel(
      title: 'Desarrollo de aplicaciones web 4',
      category: CategoryCourseModel.web,
    ),
  ].obs;

  List<CourseModel> get courseListModel => _courseListModel;
  static CoursesController get to => Get.find();

  removeCourse(int index) {
    _courseListModel.removeAt(index);
  }

  saveCourse(CourseModel course) async {
    var index = _courseListModel
        .indexWhere((element) => element.identifier == course.identifier);
    if (index == -1) {
      _courseListModel.add(course);
    } else {
      _courseListModel[index] = course;
    }
  }
}
