import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:continental_app/courses/models/course_model.dart';

import 'package:continental_app/courses/controllers/course_detail_controller.dart';

import 'package:continental_app/courses/views/widgets/error_dialog_view.dart';

class CourseDetailView extends GetView<CourseDetailController> {
  const CourseDetailView({this.course, super.key});

  final CourseModel? course;

  @override
  Widget build(BuildContext context) {
    CourseDetailController.to.loadCourse(course);
    return Obx(() => _screenUI(context));
  }

  _screenUI(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          TextField(
            controller: CourseDetailController.to.nameController,
            maxLength: 100,
            decoration: const InputDecoration(
              label: Text('Nombre del curso:'),
            ),
          ),
          Row(
            children: [
              const Text('Tipo de categoria:'),
              const SizedBox(width: 10),
              DropdownButton(
                onChanged: (value) {
                  CourseDetailController.to.onChangeCategory(value);
                },
                value: CourseDetailController.to.selectedCategory.value,
                items: CourseDetailController.to.categories
                    .map((category) => DropdownMenuItem(
                          value: category,
                          child: Text(category.name.toUpperCase()),
                        ))
                    .toList(),
              ),
            ],
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: const Color.fromARGB(255, 196, 59, 59)),
            onPressed: () {
              _saveAndPop(context);
            },
            child: const Text('Guardar'),
          )
        ],
      ),
    );
  }

  Future<void> _saveAndPop(BuildContext context) async {
    var result = await CourseDetailController.to.onSaveCategory();
    if (!context.mounted) {
      return;
    }
    if (result) {
      Navigator.pop(context);
    } else {
      showErrorDialog(context);
    }
  }
}
