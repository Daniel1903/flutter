import 'package:flutter/material.dart';

import 'package:continental_app/courses/models/course_model.dart';
import 'package:continental_app/courses/models/category_course_model.dart';

import 'package:continental_app/courses/views/course_detaill_view.dart';

class CoursesItemView extends StatelessWidget {
  const CoursesItemView(this.course, {super.key});

  final CourseModel course;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        child: Row(
          children: [
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(course.category.icon),
              ),
            ),
            Text(course.title),
          ],
        ),
      ),
      onTap: () async {
        openEditCourseOverlay(context, course);
      },
    );
  }

  openEditCourseOverlay(
    BuildContext context,
    CourseModel course,
  ) {
    showModalBottomSheet(
        useSafeArea: true,
        isScrollControlled: true,
        context: context,
        builder: (ctx) => CourseDetailView(course: course));
  }
}
