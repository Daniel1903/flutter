import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:continental_app/courses/controllers/courses_controller.dart';

import 'package:continental_app/courses/views/widgets/course_item_view.dart';

class CoursesListView extends StatelessWidget {
  const CoursesListView({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: ListView.builder(
          itemCount: CoursesController.to.courseListModel.length,
          itemBuilder: (ctx, index) => Dismissible(
            background: Container(
              color: Colors.redAccent,
              margin: const EdgeInsets.symmetric(horizontal: 16),
            ),
            onDismissed: (direction) {
              CoursesController.to.removeCourse(index);
            },
            key: ValueKey(CoursesController.to.courseListModel[index]),
            child: CoursesItemView(CoursesController.to.courseListModel[index]),
          ),
        ),
      ),
    );
  }
}
