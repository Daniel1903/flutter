import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:continental_app/courses/controllers/courses_controller.dart';

import 'package:continental_app/courses/views/course_detaill_view.dart';
import 'package:continental_app/courses/views/widgets/courses_header_view.dart';
import 'package:continental_app/courses/views/widgets/courses_list_view.dart';

class CoursesView extends GetView<CoursesController> {
  const CoursesView({super.key});

  @override
  Widget build(BuildContext context) {
    openAddCourseOverlay() {
      showModalBottomSheet(
          useSafeArea: true,
          isScrollControlled: true,
          context: context,
          builder: (ctx) => const CourseDetailView());
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Instituto Continental'),
        backgroundColor: const Color.fromARGB(255, 196, 59, 59),
        actions: [
          IconButton(
            onPressed: openAddCourseOverlay,
            icon: const Icon(Icons.add),
          )
        ],
      ),
      body: const Column(
        children: [
          CoursesHeaderView(),
          Expanded(
            child: CoursesListView(),
          )
        ],
      ),
    );
  }
}
