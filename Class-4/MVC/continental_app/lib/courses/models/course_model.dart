import 'package:continental_app/courses/models/category_course_model.dart';

class CourseModel {
  final String identifier;
  String title;
  CategoryCourseModel category;

  CourseModel({
    required this.title,
    required this.category,
  }) : identifier = uuid.v4();
}
