import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

const uuid = Uuid();

enum CategoryCourseModel { movil, web, cloud }

extension CategoryCourseModelExtension on CategoryCourseModel {
  IconData get icon {
    switch (this) {
      case CategoryCourseModel.movil:
        return Icons.mobile_friendly;
      case CategoryCourseModel.web:
        return Icons.web;
      case CategoryCourseModel.cloud:
        return Icons.filter_drama_outlined;
    }
  }
}
