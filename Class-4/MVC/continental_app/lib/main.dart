import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:continental_app/courses/controllers/courses_controller.dart';
import 'package:continental_app/courses/controllers/course_detail_controller.dart';

import 'package:continental_app/courses/views/courses_view.dart';

void main() {
  Get.put(CoursesController());
  Get.put(CourseDetailController());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: CoursesView(),
    );
  }
}
