import 'package:continental_app/domain/models/category_course_model.dart';
import 'package:flutter/material.dart';

import 'package:continental_app/presentation/courses/views/widgets/courses_item.dart';

import '../../../../domain/models/course_model.dart';

class CoursesList extends StatefulWidget {
  const CoursesList(this.onSelect, {super.key});

  final void Function() onSelect;

  @override
  State<CoursesList> createState() => _CoursesListState();
}

class _CoursesListState extends State<CoursesList> {
  final List<CourseModel> courseListModel = [
    CourseModel(category: CategoryCourseModel.movil, title: 'Mobile'),
    CourseModel(category: CategoryCourseModel.cloud, title: 'Cloud'),
  ];
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListView.builder(
        itemCount: courseListModel.length,
        itemBuilder: (ctx, index) => Dismissible(
          background: Container(
            color: Colors.redAccent,
            margin: const EdgeInsets.symmetric(horizontal: 16),
          ),
          key: ValueKey(courseListModel[index]),
          child: CoursesItem(
            courseListModel[index],
            widget.onSelect,
          ),
        ),
      ),
    );
  }
}
