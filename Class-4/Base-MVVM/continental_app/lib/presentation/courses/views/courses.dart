import 'package:flutter/material.dart';

import 'package:continental_app/presentation/courses/views/widgets/courses_header.dart';
import 'package:continental_app/presentation/courses/views/widgets/courses_list.dart';

class Courses extends StatefulWidget {
  const Courses({super.key});

  @override
  State<Courses> createState() {
    return _CoursesState();
  }
}

class _CoursesState extends State<Courses> {
  void _openDetailCourse() {}

  List<Widget>? _barActions() {
    return [
      IconButton(
        onPressed: () {},
        icon: const Icon(Icons.add),
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Instituto Continental'),
        backgroundColor: const Color.fromARGB(255, 196, 59, 59),
        actions: _barActions(),
      ),
      body: _ui(),
    );
  }

  _ui() {
    return Column(
      children: [
        const CoursesHeader(),
        Expanded(
          child: CoursesList(_openDetailCourse),
        )
      ],
    );
  }
}
