import 'package:flutter/material.dart';

import 'package:continental_app/domain/models/category_course_model.dart';

class CourseDetail extends StatefulWidget {
  const CourseDetail({super.key});

  @override
  State<CourseDetail> createState() {
    return _CourseDetailState();
  }
}

class _CourseDetailState extends State<CourseDetail> {
  final _nameController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          TextField(
            controller: _nameController,
            maxLength: 100,
            decoration: const InputDecoration(
              label: Text('Nombre del curso:'),
            ),
          ),
          Row(
            children: [
              const Text('Tipo de categoria:'),
              const SizedBox(width: 10),
              DropdownButton(
                items: CategoryCourseModel.values
                    .map((category) => DropdownMenuItem(
                          value: category,
                          child: Text(category.name.toUpperCase()),
                        ))
                    .toList(),
                value: CategoryCourseModel.web,
                onChanged: (value) {
                  if (value == null) {
                    return;
                  }
                  setState(() {});
                },
              ),
            ],
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: const Color.fromARGB(255, 196, 59, 59)),
            onPressed: () async {},
            child: Text('Save Course'),
          )
        ],
      ),
    );
  }
}
