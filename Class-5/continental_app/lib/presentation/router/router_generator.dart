import 'package:flutter/material.dart';

import 'package:continental_app/presentation/courses/pages/courses_page.dart';
import 'package:continental_app/presentation/courses/pages/course_detail_page.dart';

class RouterGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // final arguments = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const CoursesPage());
      case '/detail':
        // if (arguments is String)
        return MaterialPageRoute(builder: (_) => const CourseDetailPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
