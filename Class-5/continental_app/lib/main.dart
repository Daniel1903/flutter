import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:continental_app/presentation/router/router_generator.dart';

import 'package:continental_app/presentation/courses/courses_builder.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CoursesBuilder.builder()),
      ],
      child: const MaterialApp(
        initialRoute: '/',
        onGenerateRoute: RouterGenerator.generateRoute,
      ),
    );
  }
}
