import 'package:continental_app/presentation/courses/pages/course_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:continental_app/presentation/courses/view_models/courses_view_model.dart';

import 'package:continental_app/presentation/courses/pages/views/courses_header.dart';
import 'package:continental_app/presentation/courses/pages/views/courses_list.dart';

import 'package:continental_app/presentation/common_components/app_loading.dart';

class CoursesPage extends StatefulWidget {
  const CoursesPage({super.key});

  @override
  State<CoursesPage> createState() {
    return _CoursesPageState();
  }
}

class _CoursesPageState extends State<CoursesPage> {
  List<Widget>? _barActions(
      CoursesViewModel coursesViewModel, BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          coursesViewModel.setNewCourse();
          _navigateToDetail(context);
        },
        icon: const Icon(Icons.add),
      )
    ];
  }

  _navigateToDetail(BuildContext context) {
    // Navigator.of(context).pushNamed('/detail', arguments: );
    // Navigator.of(context).pushNamed('/detail');
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const CourseDetailPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    CoursesViewModel coursesViewModel = context.watch<CoursesViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Instituto Continental'),
        backgroundColor: const Color.fromARGB(255, 196, 59, 59),
        actions: _barActions(coursesViewModel, context),
      ),
      body: _ui(coursesViewModel),
    );
  }

  _ui(CoursesViewModel coursesViewModel) {
    if (coursesViewModel.loading) {
      return const AppLoading();
    }

    if (coursesViewModel.courseError != null) {
      return Container();
    }

    return Column(
      children: [
        const CoursesHeader(),
        Expanded(
          child: CoursesList(
            coursesViewModel.courseListModel,
            () {
              _navigateToDetail(context);
            },
          ),
        )
      ],
    );
  }
}
