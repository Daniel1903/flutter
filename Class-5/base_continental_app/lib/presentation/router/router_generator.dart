import 'package:flutter/material.dart';

import 'package:continental_app/presentation/courses/pages/courses_page.dart';
import 'package:continental_app/presentation/courses/pages/course_detail_page.dart';

class RouterGenerator {
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
