import 'package:continental_app/presentation/courses/pages/courses_page.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:continental_app/presentation/router/router_generator.dart';

import 'package:continental_app/presentation/courses/courses_builder.dart';

import 'presentation/courses/view_models/courses_view_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CoursesViewModel()),
      ],
      child: const MaterialApp(
        home: CoursesPage(),
      ),
    );
  }
}
