import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:continental_app/utils/constants.dart';

import 'package:continental_app/domain/services/icourse_service.dart';

import 'package:continental_app/domain/models/course_model.dart';
import 'package:continental_app/domain/models/error_service_model.dart';

class BDCourseService implements ICourseService {
  Future<Database>? _database;

  // Future<Database> get __database {
  //   _database ??= _getDatabase();
  //   return _database!;
  // }

  // Future<Database> _getDatabase() async {
  //   var path = await getDatabasesPath();
  //   return await openDatabase(
  //     join(path, kDataBaseName),
  //     onCreate: (db, version) => db.execute(
  //         'CREATE TABLE $kCourseDataBaseTable(id TEXT PRIMARY KEY, name TEXT, category TEXT)'),
  //     version: kDataBaseVersion,
  //   );
  // }

  // @override
  // Future<List<Object>> getCourses(Map<String, dynamic>? queries) async {
  //   try {
  //     final database = await __database;
  //     final jsonList = await database.query(kCourseDataBaseTable);
  //     return jsonList.map((item) => CourseModel.fromJson(item)).toList();
  //   } on ArgumentError {
  //     throw ErrorServiceModel(kParseError);
  //   } catch (exception) {
  //     throw ErrorServiceModel(kGenericError);
  //   }
  // }

  // @override
  // Future<bool> addCourse(Map<String, dynamic> body) async {
  //   try {
  //     final database = await __database;
  //     body['id'] = uuid.v4();
  //     await database.insert(kCourseDataBaseTable, body);
  //     return true;
  //   } catch (exception) {
  //     throw ErrorServiceModel(kGenericError);
  //   }
  // }

  // @override
  // Future<bool> updateCourse(String id, Map<String, dynamic> body) async {
  //   try {
  //     final database = await __database;
  //     await database.update(
  //       kCourseDataBaseTable,
  //       body,
  //       where: 'id = ?',
  //       whereArgs: [id],
  //     );
  //     return true;
  //   } catch (exception) {
  //     throw ErrorServiceModel(kGenericError);
  //   }
  // }
}
